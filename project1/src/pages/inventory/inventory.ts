import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ProductPage } from '../product/product';

/*
  Generated class for the Inventory page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-inventory',
  templateUrl: 'inventory.html'
})
export class InventoryPage {

  name: any;
  pID: any;
  pName: any;
  items: any;

    constructor(public navCtrl: NavController,private navParams: NavParams) {

      this.items = [];

      this.name = navParams.get('name')
      this.pID = navParams.get('pID')
      this.pName = navParams.get('pName')


      this.items.push({
        id: this.pID,
        name: this.pName
      })
    }

    redirectProductPage(){
      this.navCtrl.push(ProductPage);
    }

    itemDelete(item){
      var i ;
      for(i = 0; i < this.items.length; i++) {

        if(this.items[i] == item){
          this.items.splice(i, 1);
        }
      }
    }

    itemEdit(item){
      this.navCtrl.push(ProductPage);
    }

}
