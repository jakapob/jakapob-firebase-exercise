import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { InventoryPage } from '../inventory/inventory';

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

inventName: any;
items: any;

myDate = new Date();
toDayDate: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
    this.items = [];

    var date = this.myDate.getDate();
    var month = this.myDate.getMonth()+1;
    var year = this.myDate.getFullYear();
    this.toDayDate = date+"-"+month+"-"+year;
  }

 addInventory(){

   let prompt = this.alertCtrl.create({
     title: 'Add Inventory',
     inputs: [
       {
         name: 'inventoryName',
         placeholder: 'Inventory Name'
       },
     ],
     buttons: [
       {
         text: 'Cancel',
         handler: data => {
           console.log('Cancel clicked');
         }
       },
       {
         text: 'Submit',
         handler: data => {
           console.log('Submit clicked');
           this.inventName = data.inventoryName;

           this.items.push({
             name: this.inventName
           })
         }
       }
     ]
   });
   prompt.present();
 }

 itemTapped(event, item) {
   // That's right, we're pushing to ourselves!
   this.navCtrl.push(InventoryPage, {
     name: this.inventName
   });
 }

}
