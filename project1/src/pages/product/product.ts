import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { InventoryPage } from '../inventory/inventory';

/*
  Generated class for the Product page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-product',
  templateUrl: 'product.html'
})
export class ProductPage {

  public productID: any;
  public productName: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  save() {
    this.navCtrl.push(InventoryPage,{
      pID: this.productID,
      pName: this.productName
    })
  }

}
